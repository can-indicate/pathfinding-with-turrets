extends Control


signal _item_select
signal _not_item_select

func _process(delta):
	pass
	
	
func button_pressed(value):
	get_node("/root/level").set_item_select(value)

func _on_TextureButton3_pressed():
	button_pressed(3)


func _on_TextureButton2_pressed():
	button_pressed(2)


func _on_TextureButton_pressed():
	button_pressed(1)


func _on_ItemSelect_mouse_entered():
	emit_signal('_item_select')


func _on_ItemSelect_mouse_exited():
	emit_signal('_not_item_select')
