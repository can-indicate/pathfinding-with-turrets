extends KinematicBody2D


var velocity = Vector2(0.0, 0.0)

func initialize(position, velocity):
	self.global_position = position
	self.velocity = velocity

func remove_from_word():
	queue_free()

func _physics_process(delta):
	self.velocity = move_and_slide(self.velocity)
	if get_slide_count() > 0:
		remove_from_word()