extends Area2D

# extends KinematicBody2D


var timer = 0.0
var TIMER_END = 0.05
var body = null
var body_ref
const MUZZLE_VELOCITY = 250
const DAMAGE = 10
const Bullet = preload("res://scenes/bullet.tscn")

func _ready():
	TIMER_END += randf()
	pass

func _process(delta):
	if timer < TIMER_END:
		timer += delta
	if timer >= TIMER_END:
		if body and body_ref.get_ref():
			var body_position = body.get_position()
			var r = body_position - global_position
			look_at(body_position)
			r = r.normalized()
			var bullet = Bullet.instance()
			get_parent().add_child(bullet)
			bullet.initialize($Position2D.global_position,
							  r*MUZZLE_VELOCITY)
		timer = 0.0

func _on_Area2D_body_entered(body):
	if body.has_method("_draw"):
		self.body = body
		self.body_ref = weakref(body)
