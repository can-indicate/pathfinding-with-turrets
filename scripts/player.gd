
extends RigidBody2D

# travel speed in pixel/s
export var speed = 200

# at which distance to stop moving
# NOTE: setting this value too low might result in jerky movement near destination
const eps = 1.5

# points in the path
var points = []
var food_item
var random_number
var rng

func _ready():
	food_item = get_node("../FoodItem")
	rng = RandomNumberGenerator.new()
	set_physics_process(true)

func _physics_process(delta):
	# refresh the points in the path
	points = get_node("../Navigation2D").get_simple_path(
	get_global_position(), food_item.get_global_position(), false)
	# if the path has more than one point
	if points.size() > 1:
		var distance = points[1] - get_global_position()
		var direction = distance.normalized() # direction of movement
		if distance.length() > eps or points.size() > 2:
			set_linear_velocity(direction*speed)
		else:
			queue_free()
			# set_linear_velocity(Vector2(0, 0)) # close enough - stop moving
		update() # we update the node so it has to draw it self again


func _draw():
	# if there are points to draw
	if points.size() > 1:
		for p in points:
			pass
			# draw_circle(p - get_global_position(), 8, Color(1, 0, 0)) # we draw a circle (convert to global position first)


func _on_Area2D_body_entered(body):
	if body.has_method("remove_from_word"):
		queue_free()
