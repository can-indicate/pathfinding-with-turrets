extends Node2D


var timer = 0.0
var TIMER_END = 1.0
var tile_map
var item_select
var turret_placement_cooldown = 0.0;
var number_of_turrets = 2
var turret_image = preload("res://assets/turret_small.png")
var obstacle_grass_image = preload("res://assets/obstacle-grass.png")
var floor_image = preload("res://assets/floor.png")
const Player = preload("res://scenes/player.tscn")
const Turret = preload("res://scenes/turret.tscn")
# const Tiles = preload("res://tileset.tres")


func _ready():
	pass
	
func place_turret():
	var turret = Turret.instance()
	
func set_item_select(num: int):
	if num == 1:
		Input.set_custom_mouse_cursor(floor_image, 0, Vector2(25, 25))
	elif num == 2:
		Input.set_custom_mouse_cursor(obstacle_grass_image, 0, Vector2(25, 25))
	elif num == 3:
		Input.set_custom_mouse_cursor(turret_image, 0, Vector2(25, 25))
	self.item_select = num
	
func _input(event):
	if event.is_action_pressed("ui_up"):
		return
	elif event.is_action_pressed("ui_down"):
		return
	elif event.is_action_pressed("ui_left"):
		return
	elif event.is_action_pressed("ui_right"):
		return
	if not (event.position.x < 747 and event.position.x > 426 and 
		event.position.y > 450 and event.position.y < 598):
		if event is InputEventMouseButton:
			tile_map = get_node("/root/level/Navigation2D/TileMap")
			if event.button_index == BUTTON_LEFT:
				if item_select == 1:
					tile_map.set_cellv(tile_map.world_to_map(event.position), 0)
				elif item_select == 2:
					tile_map.set_cellv(tile_map.world_to_map(event.position), 1)
				elif item_select == 3:
					if turret_placement_cooldown <= 0.0 and number_of_turrets > 0:
						var turret = Turret.instance()
						add_child(turret)
						turret_placement_cooldown += 1.0
						turret.set_global_position(event.position)
						number_of_turrets -= 1
						get_node("/root/level/ItemSelect/TextureButton3/TextEdit").set_text(
						str(number_of_turrets))
			elif event.button_index == BUTTON_RIGHT:
				tile_map.set_cellv(tile_map.world_to_map(event.position), 0)
			# print(tile_map)
			#var turret = Turret.instance()
			#add_child(turret)
			#turret.set_global_position(event.position)

func _process(delta):
	turret_placement_cooldown -= 0.05 if turret_placement_cooldown > 0.0 else 0.0
	if timer > TIMER_END:
		var new_player = Player.instance()
		new_player.set_global_position(Vector2(250, 500))
		add_child(new_player)
		timer = 0.0
	else:
		timer += delta
		
	
