# Godot Pathfinding with Turrets
This project is based on [godot-pathfinding2d-demo](https://github.com/FEDE0D/godot-pathfinding2d-demo)
by Federico Pacheco. We are in no way endorsed or connected with him.

A live version of this game is hosted on [itch.io](https://can-indicate.itch.io/godot-pathfinding-example-with-turrets?secret=89o457iA25tDJLE2pFuRVnxzq4Y).
